document.addEventListener('DOMContentLoaded', () => {
    console.log('App initialized.');

    const addRuleButton = document.getElementById('add-rule');

    addRuleButton.addEventListener('click', () => {
        // Code for adding a new rule will go here
        console.log('Add Rule button clicked');
    });
});
